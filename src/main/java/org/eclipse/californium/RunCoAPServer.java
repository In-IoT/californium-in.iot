package org.eclipse.californium;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.SocketException;

import org.eclipse.californium.server.CoAPServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
//@EnableEurekaClient
@EnableFeignClients
public class RunCoAPServer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		// System.out.println("Execute this");
		return application.sources(RunCoAPServer.class);
	}

	public static void main(String[] args) {
		checkIfConfigFileExists();
		//HelloWorldServer.
		SpringApplication.run(RunCoAPServer.class, args);
		
		try {
			CoAPServer server = new CoAPServer();

			server.start();

		} catch (SocketException e) {
			System.err.println("Failed to initialize server: " + e.getMessage());
//			System.out.println("I am dead");
		}
	}
	
	private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write("server.port = 5683\n" + 
						"spring.main.web-application-type=NONE \n" + 
						"\n" + 
						"\n" + 
						"# serivce name\n" + 
						"spring.application.name=coap-instances\n" + 
						"# port\n" + 
						"# eureka server url\n" + 
						"#eureka.client.service-url.default-zone=http://localhost:8761/eureka\n" + 
						"eureka.client.serviceUrl.defaultZone=http://inIoTTest:lucasabbadeWare@localhost:8761/eureka/\n" + 
						"#Para deixar de dar erro quando desligada a app\n" + 
						"spring.cloud.refresh.enabled=false\n" + 
						"\n" + 
						"#spring.http.multipart.max-file-size=10MB\n" + 
						"#spring.http.multipart.max-request-size=10MB\n" + 
						"\n" + 
						"\n" + 
						"#eureka.client.enabled= false\n" + 
						"eureka.client.registerWithEureka= true\n" + 
						"eureka.client.fetchRegistry= true");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
}
