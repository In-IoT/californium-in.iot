package org.eclipse.californium.server;

import static org.eclipse.californium.constants.ConfigClass.PREFIX;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.eclipse.californium.constants.ConfigClass;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.model.Credentials;
import org.springframework.http.ResponseEntity;

import feign.FeignException;

public class LoginResource extends CoapResource {

	public LoginResource() {

		//
		// set resource identifier
		super("login");
		
		
		// set display name
		getAttributes().setTitle("Login Resource");
	}

	@Override
	public void handlePOST(CoapExchange exchange) {
		
		

		Credentials credential = getEmailAndPassword(exchange.getRequestText());
		
		if(credential == null) {
			exchange.respond(ResponseCode.BAD_REQUEST, "{\"Message\": \"The request is not in JSON format or Credentials (Username or password) were not provided\"}", MediaTypeRegistry.APPLICATION_JSON);
			return;
		}
		ResponseEntity<String> response = null;
		try{
			if(PREFIX.length()>1)
				response = ConfigClass.deviceAPIProxyEureka.login(credential);
			else
				response = ConfigClass.deviceAPIProxyNoEureka.login(credential);
		} catch(FeignException fe){
			exchange.respond(ResponseCode.FORBIDDEN, "{\"Message\": \"Invalid Username or Password\"}", MediaTypeRegistry.APPLICATION_JSON);
			return;
		}

		if (response.getStatusCodeValue() == 403) {
			exchange.respond(ResponseCode.FORBIDDEN, "{\"Message\": \"Invalid Username or Password\"}", MediaTypeRegistry.APPLICATION_JSON);
			return;
		} 
			String authorization = response.getHeaders().getFirst("Authorization");
			exchange.respond(ResponseCode.VALID, authorization, MediaTypeRegistry.APPLICATION_JSON);
		

	}

	private Credentials getEmailAndPassword(String receivedString) {

		JSONObject jsonObject = null;

		try {
			jsonObject = new JSONObject(receivedString);
		} catch (JSONException err) {
			return null;
		}

		Credentials credential = new Credentials();
		try {
			credential.setUsername(jsonObject.getString("username"));
		} catch (JSONException e) {
			return null;
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		try {
			credential.setPassword(jsonObject.getString("password"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return null;
		}
		return credential;

	}

}