package org.eclipse.californium.server;

import static org.eclipse.californium.constants.ConfigClass.PREFIX;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.eclipse.californium.constants.ConfigClass;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.springframework.http.ResponseEntity;

import feign.FeignException;

public class MessageResource extends CoapResource {

	public MessageResource() {

		//
		// set resource identifier
		super("message");
		// set display name
		getAttributes().setTitle("Message Resource");
	}

	@Override
	public void handlePOST(CoapExchange exchange) {

		JSONObject jsonObject = extractMessage(exchange.getRequestText());
		
		if(jsonObject == null) {
			exchange.respond(ResponseCode.BAD_REQUEST, "{\"Message\": \"The message is not in JSON Format\"}", MediaTypeRegistry.APPLICATION_JSON);
			return;
		}
		
		
		
		String token = jsonObject.remove("Authorization").toString();
		if(token == null) {
			exchange.respond(ResponseCode.BAD_REQUEST, "{\"Message\": \"Please include the Authorization token\"}", MediaTypeRegistry.APPLICATION_JSON);
			return;
		}
		
		if(jsonObject.toString().length() < 2 ) {
			exchange.respond(ResponseCode.BAD_REQUEST, "{\"Message\": \"Please include other variables in the Request\"}", MediaTypeRegistry.APPLICATION_JSON);
			return;
		}
//		message.setAuthorization(null);


		ResponseEntity<String> response = null;
		
		try{
			if(PREFIX.length()>1)
				response = ConfigClass.deviceAPIProxyEureka.message(token, jsonObject.toString());
			else
				response = ConfigClass.deviceAPIProxyNoEureka.message(token, jsonObject.toString());
		} catch(FeignException fe){
			exchange.respond(ResponseCode.FORBIDDEN, "{\"Message\": \"Invalid Token\"}", MediaTypeRegistry.APPLICATION_JSON);
			return;
		}

		exchange.respond(ResponseCode.CREATED, "", MediaTypeRegistry.APPLICATION_JSON);
		

	}

	private JSONObject extractMessage(String receivedString) {
		
		JSONObject jsonObject = null;

		try {
			jsonObject = new JSONObject(receivedString);
		} catch (JSONException err) {
			return null;
		}
		//jsonObject.remove("Authorization");

		return jsonObject;//.toString();
		//return Message;

	}

}