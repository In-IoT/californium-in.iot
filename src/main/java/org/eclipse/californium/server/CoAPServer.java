package org.eclipse.californium.server;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import org.eclipse.californium.constants.ConfigClass;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class CoAPServer extends CoapServer {
	
//	private static final int COAP_PORT = NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT);
	private static final int COAP_PORT = 5321;//NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT);
	/*
	 * Application entry point.
	 */
	

	@Autowired
	public CoAPServer() throws SocketException {

		addEndpoints();
		add(new LoginResource());
		add(new MessageResource());
	}
	
	private void addEndpoints() {
    	for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
    		// only binds to IPv4 addresses and localhost
			if (addr instanceof Inet4Address || addr.isLoopbackAddress()) {
				InetSocketAddress bindToAddress = new InetSocketAddress(addr, ConfigClass.port);
				addEndpoint(new CoapEndpoint(bindToAddress));
			}
		}
    }

	
}
