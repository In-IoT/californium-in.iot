package org.eclipse.californium.constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.eclipse.californium.feign.DeviceAPIProxyEureka;
import org.eclipse.californium.feign.DeviceAPIProxyNoEureka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;


@Component("ConfigClass")
public class ConfigClass {
	
	@Autowired
	public static DeviceAPIProxyNoEureka deviceAPIProxyNoEureka;
	
	@Autowired
	public static DeviceAPIProxyEureka deviceAPIProxyEureka;

	
//	@Autowired
//	public static DeviceAPIProxyNoEureka deviceAPIProxy;
	
	public static final int port;// = 5432;
	
	@Autowired
    public static DiscoveryClient discoveryClient;
	
	public static final String PREFIX;
	
//	public static String forwarPort;
//	public static int runningPort;
//	public static boolean isMultipleMQTTInstances;
//	public static ArrayList<ForwardAddressAndPort> forwardAddressAndPort = new ArrayList<>();
//	
//	public static String dashboardREST;
//	public static String DeviceREST;
//	
//	public static String BrokerUsername;
//	public static String BrokerPassword;
//	
//	public static String restSignInURL;
//	public static String dashboardSignInURL;
//	public static String restPublishURL;
//	
//	public static String ipAddress;
	
	@Autowired
    public ConfigClass(DeviceAPIProxyNoEureka deviceAPIProxyNoEureka, 
    		DeviceAPIProxyEureka deviceAPIProxyEureka) {//,
    		//DiscoveryClient discoveryClient){
		
		ConfigClass.deviceAPIProxyNoEureka = deviceAPIProxyNoEureka;
		ConfigClass.deviceAPIProxyEureka = deviceAPIProxyEureka;
    }

	static {
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "application.properties";
		
		try {
			inputStream = new FileInputStream (System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
    	String prefixAux = prop.getProperty("eureka.client.register-with-eureka", "true");
    	if(prefixAux.equals("true")) {
    		PREFIX = "/admin-gui";
    	} else {
    		PREFIX = "";
    	}
    	
    	//System.out.println(PREFIX);
    	port = Integer.parseInt(prop.getProperty("server.port", "5683"));
    }
	
	
	
	

}
