package org.eclipse.californium.testing;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.Request;

public class MessageClient {

	/*
	 * Application entry point.
	 * 
	 */
	public static void main(String args[]) throws URISyntaxException {

		URI uri = new URI("localhost:5683/message"); // URI parameter of the request

		CoapClient client = new CoapClient(uri);

		Request request = new Request(Code.POST);
		request.setPayload("{"
				+ "\"Authorization\": \"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2NzUxMWNjZS03MmNiLTRkMjYtYmU2Mi0yODE4NzMxNTQ0MGQiLCJpc3MiOiI1YmUzLTNiOGQtOTVmZS1kZTEwLTM5NTEtMDg1YSIsImV4cCI6MTU2MTIxMDE2NX0.V20Tz_fR5-fTONPbZjYTeVmKeAZyyoyN1NHwCFOAlVM\","
				+ "\"unit\":\"1\","
				+ "\"unit2\":\"2\"}");
//		OptionSet options = new OptionSet();
//		Option option = new Option();
//		option.
//		//option.
//		//option.setStringValue("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YTFmODU1NS1kOWMyLTQ4OTAtOWE0ZC0zZDJhZWFhZDNlN2MiLCJpc3MiOiI1Y2VmLWMzYjktYjAzNS1mZjBjLWQyMzMtOTJjMiIsImV4cCI6MTU2MDYyOTkwM30.7slHCtm6BaKReZpuQBMaE1oWJnQaFMTr9DSqRq_Jxw0");
//		options.addOption(option);
//		request.setOptions(options);
		//options.addOption(option)
		//request.setOptions()
		//request.getOptions().
		//request.setToken(new String ("123456789").getBytes());
		CoapResponse response = client.advanced(request);

		// CoapResponse response = client.get();

		if (response != null) {

			System.out.println(response.getCode());
			System.out.println(response.getOptions());

			System.out.println(response.getResponseText());

			System.out.println(System.lineSeparator() + "ADVANCED" + System.lineSeparator());

			System.out.println(Utils.prettyPrint(response));
		} else {
			System.out.println("No response received.");
		}

	}

}