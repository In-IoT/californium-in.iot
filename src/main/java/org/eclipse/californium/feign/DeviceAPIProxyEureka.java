package org.eclipse.californium.feign;

import org.eclipse.californium.model.Credentials;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "device-api")
//@RibbonClient(name="admin-api")
public interface DeviceAPIProxyEureka {

	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/auth/device/signin", consumes = "application/json")
	ResponseEntity<String> login(Credentials credential);
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/message", consumes = "application/json")
	ResponseEntity<String> message(@RequestHeader("Authorization") String token,String message);

}